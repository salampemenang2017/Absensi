<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaKelas extends Model
{
    protected $table = 'siswa_kelas';
    protected $guarded = ['siswa_kelas_id'];
    protected $primaryKey = 'siswa_kelas_id';

}
