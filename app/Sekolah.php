<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $table = 'sekolah';
    protected $fillable = [
    						'nama',
    						'npsn',
    						'logo', 
    						'kode_un', 
    						'alamat', 
    						'no_sk_pendirian_sekolah', 
    						'tgl_pendirian'];

    protected $primaryKey = 'sekolah_id';
}
