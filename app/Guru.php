<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';
    protected $fillable = [
    						'nama',
    						'nip',
    						'jenis_kelamin',
    						'alamat', 
    						'sekolah_id', 
						];

    protected $primaryKey = 'guru_id';

}
