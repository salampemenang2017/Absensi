<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = [
    						'nama',
    						'nis',
    						'nisn', 
    						'nik', 
    						'no_akte', 
    						'no_kk', 
    						'jenis_kelamin',
    						'agama',
    						'alamat',
    						'no_hp_ortu',
                            'siswa_id'];

	protected $primaryKey = 'siswa_id';

}
