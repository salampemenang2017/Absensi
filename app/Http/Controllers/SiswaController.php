<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use App\Export\SiswaExport;
use App\Import\SiswaImport;

class SiswaController extends Controller
{

    public function getExport()
    {
        return view('export');
    }

    public function getImport()
    {
        return view('import');
    }

    public function SiswaExport() 
	{
		return Excel::download(new SiswaExport, 'siswa.xlsx');
    }
    
    public function SiswaImport(Request $request)
	{
		if ($request->hasFile('file')) {
			$file = $request->file('file');
            Excel::import(new SiswaImport, $file);
            return redirect()->back();
			// return redirect()->route('kelola-data-siswa');
		}        
	}
}
