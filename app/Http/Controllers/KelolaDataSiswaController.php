<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Siswa;
use DB;

class KelolaDataSiswaController extends Controller
{
    public function index(){
    	return view('kelolaDataSiswa.index');
    }
    public function getDataSiswa(){
    	$siswa = Siswa::all();

    	return Datatables::of($siswa)
    					->addColumn('action', 'kelolaDataSiswa.action')
    					->addIndexColumn()
    					->make(true);
    }
    public function showAddSiswa(){
        return view('kelolaDataSiswa.add');
    }
    public function postAddSiswa(Request $req){
        $this->validate($req, [
            'nama' => 'required|min:3',
            'nis' => 'required|unique:siswa',
            'nisn' => 'required|unique:siswa',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'no_hp_ortu' => 'required|min:8', 
        ]);

        $A = new Siswa;
        $A->nama = $req->nama;
        $A->nis = $req->nis;
        $A->nisn = $req->nisn;
        $A->nik = $req->nik;
        $A->no_akte = $req->no_akte;
        $A->no_kk = $req->no_kk;
        $A->jenis_kelamin = $req->jenis_kelamin;
        $A->agama = $req->agama;
        $A->alamat = $req->alamat;
        $A->no_hp_ortu = $req->no_hp_ortu;

        $A->save();

        return redirect()->route('kelola-data-siswa');
    }
    public function EditSiswa($Siswa_id){
        $siswa = Siswa::findOrFail($Siswa_id);

        return view('kelolaDataSiswa.edit', ['siswa'=>$siswa]);
    }
    public function updateSiswa(Request $req, $Siswa_id){
        $this->validate($req, [
            'nama' => 'required|min:3',
            'nis' => 'required',
            'nisn' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'no_hp_ortu' => 'required|min:8',
        ]);

        $A = Siswa::findOrFail($Siswa_id);
        $A->nama = $req->nama;
        $A->nis = $req->nis;
        $A->nisn = $req->nisn;
        $A->nik = $req->nik;
        $A->no_akte = $req->no_akte;
        $A->no_kk = $req->no_kk;
        $A->jenis_kelamin = $req->jenis_kelamin;
        $A->agama = $req->agama;
        $A->alamat = $req->alamat;
        $A->no_hp_ortu = $req->no_hp_ortu;

        $A->save();

        return redirect()->route('kelola-data-siswa');
    }
    public function deleteSiswa($Siswa_id){
        Siswa::findOrFail($Siswa_id)->delete();

        return redirect()->route('kelola-data-siswa');
    }
}
