<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use App\Export\GuruExport;
use App\Import\GuruImport;
use App\Guru;
use App\Sekolah;
use DataTables;
use DB;

class GuruController extends Controller
{
    public function getExport()
    {
        return view('guru.export');
    }

    public function getImport()
    {
        return view('guru.import');
    }

    public function GuruExport() 
	{
		return Excel::download(new GuruExport, 'guru.xlsx');
    }
    
    public function GuruImport(Request $request)
	{
		if ($request->hasFile('file')) {
			$file = $request->file('file');
            Excel::import(new GuruImport, $file);
            return redirect()->back();
			// return redirect()->route('kelola-data-guru');
		}        
    }
    
    public function guru()
    {
        return view('guru.index');
    }

    public function guru1A()
    {
        
        $guru = DB::table('guru')
        ->join('sekolah', 'guru.sekolah_id', '=', 'sekolah.sekolah_id')
        ->select('guru.*', 'sekolah.nama')
        ->get();
        
        
    	return Datatables::of($guru)
    					->addColumn('action', 'guru.action')
    					->addIndexColumn()
    					->make(true);
    }
    
    public function showAddGuru($id)
    {
        $guru = Guru::findOrFail($id);
        $sekolah = Sekolah::all();
        return view('guru.edit', compact('guru', 'sekolah'));
    }

    public function tambahGuru()
    {
        $sekolah = Sekolah::all();
        return view('guru.add', compact('sekolah'));
    }
    
    public function postAddGuru(Request $req)
    {
        $id = $req->get('guru_id');        
        if($id)
        {
            $A = Guru::findOrFail($id);
        }else{
            $A = new Guru;
        }
        
        
        $A->nama_guru = $req->nama_guru;
        $A->nip = $req->nip;
        $A->jenis_kelamin = $req->jenis_kelamin;
        $A->alamat = $req->alamat;
        $A->sekolah_id = $req->sekolah_id;
        $A->save();

        return redirect()->route('data-guru');
    }

    public function deleteGuru($id)
    {
        Guru::findOrFail($id)->delete();

        return redirect()->route('data-guru');
    }
}
