<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\TahunAjaran;
use DB;

class TahunAjaranController extends Controller
{
    public function index(){
    	return view('kelolaDataTahun.index');
    }

    public function getDataTahunAjaran(){
    	$tahun = TahunAjaran::all();

    	return Datatables::of($tahun)
    					->addColumn('action', 'kelolaDataTahun.action')
    					->addIndexColumn()
    					->make(true);
    }

    public function showAddTahunAjaran(){
        return view('kelolaDataTahun.add');
    }

    public function postAddTahunAjaran(Request $req){
        $this->validate($req, [
            'tahun_ajaran_nama' => 'required',
            'status' => 'required',
        ]);

        $A = new TahunAjaran;
        $A->tahun_ajaran_nama = $req->tahun_ajaran_nama;
        $A->status = $req->status;
    
        $A->save();

        return redirect()->route('kelola-data-tahunajaran');
    }

    public function EditTahunAjaran($tahunajaran_id){
        $tahun = TahunAjaran::findOrFail($tahunajaran_id);

        return view('kelolaDataTahun.edit', ['tahun'=>$tahun]);
    }

    public function updateTahunAjaran(Request $req, $tahunajaran_id){
        $this->validate($req, [
            'tahun_ajaran_nama' => 'required|min:3',
            'status' => 'required',
        ]);

        $A = TahunAjaran::findOrFail($tahunajaran_id);
        $A->tahun_ajaran_nama = $req->tahun_ajaran_nama;
        $A->status = $req->status;
        $A->save();

        return redirect()->route('kelola-data-tahunajaran');
    }

    public function deleteTahunAjaran($tahunajaran_id){
        TahunAjaran::findOrFail($tahunajaran_id)->delete();

        return redirect()->route('kelola-data-tahunajaran');
    }
}
