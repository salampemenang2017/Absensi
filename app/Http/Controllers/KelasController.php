<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use DataTables;
use App\TahunAjaran;
use App\Sekolah;
use DB;

class KelasController extends Controller
{
    public function index(){
    	return view('kelolaKelas.index');
    }
    public function getKelas(){
        $kelas = DB::table('kelas')
                ->join('sekolah', 'kelas.sekolah_id', '=' ,'sekolah.sekolah_id')
                ->select('kelas.*', 'sekolah.nama')
                ->get();

    	return Datatables::of($kelas)
    						->addColumn('action', 'kelolaKelas.action')
    						->addIndexColumn()
    						->make(true); 
    }
    public function showAddKelas(){
    	$sekolah = Sekolah::all();

    	return view('kelolaKelas.add', ['sekolah' => $sekolah]);
    }
    public function addKelas(Request $req){
    	$this->validate($req, [
    		'kode_kelas' => 'required',
    		'nama_kelas' => 'required',
    		'sekolah_id' => 'required'
    	]);

    	$k = new Kelas;
    	$k->kode_kelas = $req->kode_kelas;
    	$k->nama_kelas = $req->nama_kelas;
    	$k->sekolah_id = $req->sekolah_id;

    	$k->save();

    	return redirect()->route('kelola-kelas');
    }
    public function showEditKelas($Kelas_id){
        $kelas = DB::table('kelas')->where('kelas_id', '=', $Kelas_id)
                    ->get()
                    ->first();
        $ta = TahunAjaran::all();

        return view('kelolaKelas.edit', ['kelas'=>$kelas, 'ta'=>$ta]);
    }
    public function updateKelas(Request $req, $Kelas_id){
        $this->validate($req, [
            'kode_kelas' => 'required',
            'nama_kelas' => 'required',
            'sekolah_id' => 'required',
        ]);

        $k = Kelas::findOrFail($Kelas_id);
        $k->kode_kelas = $req->kode_kelas;
        $k->nama_kelas = $req->nama_kelas;
        $k->sekolah_id = $req->sekolah_id;

        $k->save();

        return redirect()->route('kelola-kelas');
    }
    public function DelKelas($Kelas_id){
        DB::table('kelas')->where('kelas_id', '=', $Kelas_id)->delete();
        return redirect()->route('kelola-kelas');
    }
}
