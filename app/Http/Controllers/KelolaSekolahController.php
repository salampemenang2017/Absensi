<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Http\Request;
use App\Sekolah;
use DB;
use Image;

class KelolaSekolahController extends Controller
{
    public function index(){
    	return view('kelolasekolah.index');
    }
    public function getDataSekolah(){
    	$sekolah = Sekolah::all();

    	return Datatables::of($sekolah)
    						->addColumn('action', 'kelolasekolah.action')
    						->addIndexColumn()
    						->make(true);
    }
    public function ShowTambahSekolah(){
    	return view('kelolasekolah.add');
    }
    public function ShowEditSekolah($Sekolah_id){
    	$sekolah = Sekolah::findOrFail($Sekolah_id);

    	return view('kelolasekolah.edit', ['sekolah'=>$sekolah]);
    }
    public function UpdateSekolah(Request $req, $Sekolah_id){
    	$this->validate($req, [
            'nama' => 'required|min:3',
        ]);

    	$A = Sekolah::findOrFail($Sekolah_id);
    	$A->nama = $req->nama;
    	$A->no_sk_pendirian_Sekolah = $req->no_sk_pendirian_Sekolah;
    	$A->npsn = $req->npsn;
    	$A->kode_un = $req->kode_un;
    	$A->alamat = $req->alamat;
    	$A->tgl_pendirian = $req->tgl_pendirian;

    	$A->save();

    	return redirect()->route('super.admin.kelola-sekolah');
    }
    public function PostTambahSekolah(Request $req){
    	$this->validate($req, [
            'nama' => 'required|min:3',
        ]);

    	$A = new Sekolah;
    	$A->nama = $req->nama;
    	$A->no_sk_pendirian_Sekolah = $req->no_sk_pendirian_Sekolah;
    	$A->npsn = $req->npsn;
    	$A->kode_un = $req->kode_un;
    	$A->alamat = $req->alamat;
    	$A->tgl_pendirian = $req->tgl_pendirian;

    	$A->save();

    	return redirect()->route('super.admin.kelola-sekolah');
    }
    public function DelSekolah($Sekolah_id){
    	DB::table('sekolah')->where('sekolah_id', '=', $Sekolah_id)->delete();
    	return redirect()->route('super.admin.kelola-sekolah');
    }
    public function dataPersekolah($Sekolah_id){

        $sekolah = Sekolah::findOrFail($Sekolah_id);

        return view('kelolasekolah.data-persekolah', ['sekolah'=>$sekolah]);
    }
    public function editDataPersekolah($Sekolah_id){

        $sekolah = Sekolah::findOrFail($Sekolah_id);

        return view('kelolasekolah.edit-data-persekolah', ['sekolah'=>$sekolah]);
    }
    public function updateDataPersekolah(Request $req, $Sekolah_id){
        $this->validate($req, [
            'nama' => 'required|min:3',
        ]);

        $A = Sekolah::findOrFail($Sekolah_id);
        $A->nama = $req->nama;
        $A->no_sk_pendirian_Sekolah = $req->no_sk_pendirian_Sekolah;
        $A->npsn = $req->npsn;
        $A->kode_un = $req->kode_un;
        $A->alamat = $req->alamat;
        $A->tgl_pendirian = $req->tgl_pendirian;

        $A->save();

        return redirect("admin/kelola-data-sekolah/" . $Sekolah_id);
    }
    public function updateLogoSekolah(Request $req, $Sekolah_id){
        
        $logo = Sekolah::findOrFail($Sekolah_id);

        if($req->hasFile('logo')){
            $foto = $req->file('logo');
            $filename = time() . '.' . $foto->getClientOriginalExtension();
            Image::make($foto)->save(public_path('/img/' . $filename));
        }

        $logo->logo = $filename;
        $logo->save();
        return redirect()->back();
    }
}
