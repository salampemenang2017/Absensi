<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use App\Export\SiswaKelasExport;
use App\Import\SiswaKelasImport;
use DB;
use DataTables;
use App\SiswaKelas;
use App\Kelas;
use App\Siswa;
use App\TahunAjaran;

class SiswaKelasController extends Controller
{
    public function getExport()
    {
        return view('siswakelas.export');
    }

    public function getImport()
    {
        return view('siswakelas.import');
    }

    public function SiswaKelasExport() 
	{
		return Excel::download(new SiswaKelasExport, 'siswakelas.xlsx');
    }
    
    public function SiswaKelasImport(Request $request)
	{
		if ($request->hasFile('file')) {
			$file = $request->file('file');
            Excel::import(new SiswaKelasImport, $file);
            return redirect()->back();
			// return redirect()->route('kelola-data-guru');
		}        
    }
    
    public function siswaKelas()
    {
        return view('siswakelas.index');
    }

    public function siswaKelas1A()
    {
        $kelas = Kelas::where('nama_kelas', '=', 1, 'AND', 'kode_kelas', '=', 'A')->get()->first();
        $tahun = DB::table('tahun_ajaran')->where('status', '=', 'Aktif')->get()->first();
    
        
        $siswa = DB::table('siswa_kelas')
        ->join('siswa', 'siswa_kelas.siswa_id', '=', 'siswa.siswa_id')
        ->join('kelas', 'siswa_kelas.kelas_id', '=', 'kelas.kelas_id')
        ->select('siswa_kelas.*', 'siswa.nama', 'kelas.nama_kelas', 'kelas.kode_kelas')
        ->where('tahun_ajaran_id', '=', $tahun->tahun_ajaran_id )
        ->get();
        
        
    	return Datatables::of($siswa)
    					->addColumn('action', 'siswakelas.action')
    					->addIndexColumn()
    					->make(true);
    }

    // Riwayat
    public function siswaKelasR()
    {
        $tahun = DB::table('tahun_ajaran')->where('status', '=', 'Non aktif')->get()->first();
    
        
        $siswa = DB::table('siswa_kelas')
        ->join('siswa', 'siswa_kelas.siswa_id', '=', 'siswa.siswa_id')
        ->join('kelas', 'siswa_kelas.kelas_id', '=', 'kelas.kelas_id')
        ->select('siswa_kelas.*', 'siswa.nama', 'kelas.nama_kelas', 'kelas.kode_kelas')
        ->where('tahun_ajaran_id', '=', $tahun->tahun_ajaran_id )
        ->get();
        
        
    	return Datatables::of($siswa)
    					->addIndexColumn()
    					->make(true);
    }

    public function tambahSiswaKelas()
    {
        $siswa = Siswa::all();
        $kelas = Kelas::all();
        $tahun = DB::table('tahun_ajaran')->where('status', '=', 'Aktif')->get();
        return view('siswakelas.add', compact('siswa', 'kelas', 'tahun'));
    }
    
    public function showAddSiswaKelas($id)
    {
        $siswaK = SiswaKelas::findOrFail($id);
        $siswa = Siswa::all();
        $kelas = Kelas::all();
        $tahun = TahunAjaran::all();
        return view('siswakelas.mutasi', compact('siswaK', 'siswa', 'kelas', 'tahun'));
    }
    
    public function postAddSiswaKelas(Request $req)
    {

        $this->validate($req, [
            'siswa_id' => 'required',
            'kelas_id' => 'required',
            'tahun_ajaran_id' => 'required',
        ]);

        
        $A = new SiswaKelas;
        
        
        $A->siswa_id = $req->siswa_id;
        $A->kelas_id = $req->kelas_id;
        $A->tahun_ajaran_id = $req->tahun_ajaran_id;

        $A->save();

        return redirect()->route('data-siswakelas');
    }

    public function deleteSiswaKelas($id)
    {
        SiswaKelas::findOrFail($id)->delete();

        return redirect()->route('data-siswakelas');
    }

}
