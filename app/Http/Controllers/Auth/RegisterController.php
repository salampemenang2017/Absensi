<?php

namespace App\Http\Controllers\Auth;

use App\Admin;
use App\Sekolah;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    public function RegisterShow(){
        $S = Sekolah::all();

        return view('auth.register', ['S'=>$S]);
    }
    public function Register(Request $req){
        $this->validate($req, [
            'name' => 'required|min:3',
            'email' => 'required|unique:admins',
            'password' => 'required|min:8|confirmed',
        ]);

        $reg = new Admin;
        $reg->name = $req->name;
        $reg->email = $req->email;
        $reg->sekolah_id = $req->sekolah; 
        $reg->password = Hash::make($req->password);
        $reg->save();

        if (auth()->attempt(array('email' => $req['email'], 'password' => $req['password']))){
            if (auth()->user()->is_superadmin == 1) {
                return redirect()->route('super.admin.home');
            }else{
                return redirect()->route('home');
            }
        }
    }
}
