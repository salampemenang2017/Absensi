<?php

namespace App\Http\Controllers\Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use DB;
use App\Sekolah;
use Hash;

class UserController extends Controller
{
    public function getAllAdminSJson(){
    	$Admins = DB::table('admins')
    				->where('is_superadmin', '=', null)
                    ->join('sekolah', 'sekolah.sekolah_id', '=', 'admins.sekolah_id')
    				->select('admins.*', 'sekolah.nama')
    				->get();

    	return Datatables::of($Admins)
    						->addColumn('action', 'kelolaadmin.action')
    						->addIndexColumn()
    						->make(true);
    }
    public function showEditAdmin($Admin_id){
    	$Admins = DB::table('admins')
    				->join('sekolah', 'admins.sekolah_id', '=', 'sekolah.sekolah_id')
    				->where('id', '=', $Admin_id)
    				->select('admins.*', 'sekolah.nama')
    				->get()
    				->first();
    	
    	$Sekolah = Sekolah::all();

    	return view('kelolaadmin.edit', ['Admins'=>$Admins, 'Sekolah'=>$Sekolah]);
    }
    public function updateAdmin(Request $req, $Admin_id){
    	$A = Admin::findOrFail($Admin_id);
    	$A->name = $req->name;
    	$A->email = $req->email;
    	$A->nip = $req->nip;
    	$A->jenis_kelamin = $req->jenis_kelamin;
    	$A->alamat = $req->alamat;
    	$A->sekolah_id = $req->sekolah;

    	$A->save();

    	return redirect()->route('super.admin.home');
    }
    public function showAddAdmin(Request $req){
    	$Sekolah = Sekolah::all();

    	return view('kelolaadmin.add', ['Sekolah'=>$Sekolah]);
    }
    public function postAddAdmin(Request $req){
        $this->validate($req, [
            'name' => 'required|min:3',
            'nip' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'email' => 'required|unique:admins',
            'password' => 'required|min:8|confirmed',
            'sekolah' => 'required',
        ]);

    	$A = new Admin;
    	$A->name = $req->name;
    	$A->email = $req->email;
    	$A->nip = $req->nip;
    	$A->jenis_kelamin = $req->jenis_kelamin;
    	$A->alamat = $req->alamat;
    	$A->sekolah_id = $req->sekolah;
    	$A->password = Hash::make($req->password);

    	$A->save();

    	return redirect()->route('super.admin.home');
    }
    public function DelAdmin($Admin_id){
    	DB::table('admins')->where('id', $Admin_id)->delete();
    	return redirect()->back();
    }
}
