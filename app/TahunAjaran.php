<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    protected $table = 'tahun_ajaran';
    protected $guarded = ['tahun_ajaran_id'];
    protected $primaryKey = 'tahun_ajaran_id';
}
