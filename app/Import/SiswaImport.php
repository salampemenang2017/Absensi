<?php

namespace App\Import;

use Illuminate\Database\Eloquent\Model;

use App\Siswa;

use Maatwebsite\Excel\Concerns\ToModel;

class SiswaImport implements ToModel
{
    public function model(array $row)
    {
        return new Siswa([
            'siswa_id' => $row[0],
            'nama' => $row[1],
            'nis' => $row[2],
            'nisn' => $row[3],
            'nik' => $row[4],
            'no_akte' => $row[5],
            'no_kk' => $row[6],
            'jenis_kelamin' => $row[7],
            'agama' => $row[8],
            'alamat' => $row[9],
            'no_hp_ortu' => $row[10],
            'created_at' => $row[11],
            'updated_at' => $row[12],
        ]);  
    }
}
