<?php

namespace App\Import;

use Illuminate\Database\Eloquent\Model;

use App\SiswaKelas;

use Maatwebsite\Excel\Concerns\ToModel;

class SiswaKelasImport implements ToModel
{
    public function model(array $row)
    {
        return new SiswaKelas([
            'siswa_kelas_id' => $row[0],
            'siswa_id' => $row[1],
            'kelas_id' => $row[2],
            'tahun_ajaran_id' => $row[3],
            'created_at' => $row[4],
            'updated_at' => $row[5],
        ]);  
    }
}
