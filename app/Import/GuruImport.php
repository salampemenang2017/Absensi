<?php

namespace App\Import;

use Illuminate\Database\Eloquent\Model;

use App\Guru;

use Maatwebsite\Excel\Concerns\ToModel;

class GuruImport implements ToModel
{
    public function model(array $row)
    {
        return new Guru([
        'guru_id' => $row[0],
        'nama' => $row[1],
        'nip' => $row[2],
        'jenis_kelamin' => $row[3],
        'alamat' => $row[4],
        'sekolah_id' => $row[5],
        'created_at' => $row[6],
        'updated_at' => $row[7],
      ]);  
    }
}
