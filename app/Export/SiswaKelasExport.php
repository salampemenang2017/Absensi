<?php

namespace App\Export;

use Illuminate\Database\Eloquent\Model;


use App\SiswaKelas;
use Maatwebsite\Excel\Concerns\FromCollection;

class SiswaKelasExport implements FromCollection
{
    public function collection()
    {
        return SiswaKelas::All(); 
    }
}
