<?php

namespace App\Export;

use Illuminate\Database\Eloquent\Model;

use App\Guru;

use Maatwebsite\Excel\Concerns\FromCollection;

class GuruExport implements FromCollection
{
    public function collection()
    {
        return Guru::All(); 
    }
}
