<?php

namespace App\Export;

use Illuminate\Database\Eloquent\Model;

use App\Siswa;
use Maatwebsite\Excel\Concerns\FromCollection;

class SiswaExport implements FromCollection
{
    public function collection()
    {
        return Siswa::All(); 
    }
}
