<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $fillable = [
    						'kode_kelas',
    						'kelas_id',
    						'nama_kelas',
    						'sekolah_id', 
						];

    protected $primaryKey = 'kelas_id';
}
