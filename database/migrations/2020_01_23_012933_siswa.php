<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Siswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->bigIncrements('siswa_id');
            $table->string('nama', 100);
            $table->string('nis', 20)->unique();
            $table->string('nisn', 20)->unique();
            $table->string('nik', 20)->nullable();
            $table->string('no_akte', 20)->nullable();
            $table->string('no_kk', 20)->nullable();
            $table->string('jenis_kelamin', 1);
            $table->string('agama', 10);
            $table->text('alamat')->nullable();
            $table->string('no_hp_ortu', 12)->nullable();
            $table->unsignedBigInteger('kelas_id')->nullable();
            $table->foreign('kelas_id')->references('kelas_id')->on('kelas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
