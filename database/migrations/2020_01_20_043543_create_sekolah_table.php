<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolah', function (Blueprint $table) {
            $table->bigIncrements('sekolah_id');
            $table->string('nama', 50);
            $table->string('npsn', 20)->nullable();
            $table->string('logo')->default('default.jpg');
            $table->string('kode_un', 20)->nullable();
            $table->text('alamat')->nullable();
            $table->string('no_sk_pendirian_sekolah', 50);
            $table->date('tgl_pendirian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolah');
    }
}
