<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the 'web' middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect('login');
});
//Kelola users
Route::get('/adminS_json', 'Auth\UserController@getAllAdminSJson')->name('AdminS.data');
Route::get('/delete-Admins/{Admin_id}', 'Auth\UserController@DelAdmin')->name('Del-Admin');
Route::get('/edit-admins/{Admin_id}', 'Auth\UserController@showEditAdmin')->name('edit-Admin');
Route::post('/update-admins/{Admin_id}', 'Auth\UserController@updateAdmin')->name('update-Admin');
Route::get('/add-admins/', 'Auth\UserController@showAddAdmin')->name('add-Admin');
Route::post('/add-post-admins/', 'Auth\UserController@postAddAdmin')->name('add-post-Admin');

//Authenticate
Auth::routes();
Route::get('/register', 'Auth\RegisterController@RegisterShow')->name('Register');
Route::post('/register-post', 'Auth\RegisterController@Register')->name('Register-post');


Route::prefix('super-admin')->group(function() {
	Route::get('/home', 'HomeController@superAdminHome')
			->name('super.admin.home')
			->middleware('is_superadmin');

	Route::get('/kelola-sekolah', 'KelolaSekolahController@index')
			->name('super.admin.kelola-sekolah')
			->middleware('is_superadmin');

	//get data AND CRUD Sekolah
	Route::get('/data-sekolah', 'KelolaSekolahController@getDataSekolah')
			->name('get-data-sekolah')
			->middleware('is_superadmin');
	Route::get('/delete-Sekolah/{Sekolah_id}', 'KelolaSekolahController@DelSekolah')
			->middleware('is_superadmin');
	Route::get('/show/tambah-sekolah', 'KelolaSekolahController@ShowTambahSekolah')
			->name('show.tambah-data-sekolah')
			->middleware('is_superadmin');
	Route::post('/post/tambah-sekolah', 'KelolaSekolahController@PostTambahSekolah')
			->name('post.tambah-data-sekolah')
			->middleware('is_superadmin');
	Route::get('/show/edit-sekolah/{Sekolah_id}', 'KelolaSekolahController@ShowEditSekolah')
			->name('show.edit-data-sekolah')
			->middleware('is_superadmin');
	Route::post('/update/tambah-sekolah/{Sekolah_id}', 'KelolaSekolahController@UpdateSekolah')
			->name('update.data-sekolah')
			->middleware('is_superadmin');
			
});

Route::prefix('admin')->group(function() {
	Route::get('/home', 'HomeController@index')
			->name('home')
			->middleware('is_admin');
	
	// Kelola Data Sekolah
	
	Route::get('/kelola-data-sekolah/{Sekolah_id}', 'KelolaSekolahController@editDataPerSekolah')
	->name('edit-sekolah')
	->middleware('is_admin');
	Route::post('/update-data-persekolah/{Sekolah_id}', 'KelolaSekolahController@updateDataPerSekolah')
	->name('update-sekolah')
	->middleware('is_admin');
	

	// Tahun Ajaran
	Route::prefix('tahunajaran')->group(function(){
		//Kelola Data Tahun Ajaran
		Route::get('/kelola-data-tahunajaran', 'TahunAjaranController@index')
		->name('kelola-data-tahunajaran')
		->middleware('is_admin');
		Route::get('/data-tahunajaran', 'TahunAjaranController@getDataTahunAjaran')
		->name('get-data-tahunajaran')
		->middleware('is_admin');
		Route::get('/show-data-tahunajaran', 'TahunAjaranController@showAddTahunAjaran')
		->name('show-add-tahunajaran')
		->middleware('is_admin');
		Route::post('/post-data-tahunajaran', 'TahunAjaranController@postAddTahunAjaran')
		->name('post-add-tahunajaran')
		->middleware('is_admin');
		Route::get('/delete-data-tahunajaran/{tahunajaran_id}', 'TahunAjaranController@deleteTahuAjaran')
		->name('delete-tahunajaran')
		->middleware('is_admin');
		Route::get('/show-edit-tahunajaran/{tahunajaran_id}', 'TahunAjaranController@editTahunAjaran')
		->name('edit-tahunajaran')
		->middleware('is_admin');
		Route::post('/update-tahunajaran/{tahunajaran_id}', 'TahunAjaranController@updateTahunAjaran')
		->name('update-tahunajaran')
		->middleware('is_admin');
	});

	// Kelola Data Siswa
	Route::prefix('siswa')->group(function(){
		//Kelola Data Siswa
		Route::get('/kelola-data-siswa', 'KelolaDataSiswaController@index')
		->name('kelola-data-siswa')
		->middleware('is_admin');
		Route::get('/data-siswa', 'KelolaDataSiswaController@getDataSiswa')
		->name('get-data-siswa')
		->middleware('is_admin');
		Route::get('/show-data-siswa', 'KelolaDataSiswaController@showAddSiswa')
		->name('show-add-siswa')
		->middleware('is_admin');
		Route::post('/post-data-siswa', 'KelolaDataSiswaController@postAddSiswa')
		->name('post-add-siswa')
		->middleware('is_admin');
		Route::get('/delete-data-siswa/{Siswa_id}', 'KelolaDataSiswaController@deleteSiswa')
		->name('delete-siswa')
		->middleware('is_admin');
		Route::get('/show-edit-siswa/{Siswa_id}', 'KelolaDataSiswaController@editSiswa')
		->name('edit-siswa')
		->middleware('is_admin');
		Route::post('/update-siswa/{Siswa_id}', 'KelolaDataSiswaController@updateSiswa')
		->name('update-siswa')
		->middleware('is_admin');

		// Export Import Siswa

		Route::get('/export/excel', 'SiswaController@SiswaExport')
		->middleware('is_admin')
		->name('siswa-export');

		Route::post('/import/excel', 'SiswaController@SiswaImport')
		->middleware('is_admin')
		->name('siswa-import');
		
	});

	// Kelola Kelas
	Route::prefix('kelas')->group(function(){
		Route::get('/', 'KelasController@index')
		->name('kelola-kelas')
		->middleware('is_admin');
		Route::get('/data-kelas', 'KelasController@getKelas')
				->name('get-kelas')
				->middleware('is_admin');
		Route::get('/show-data-kelas', 'KelasController@showAddKelas')
				->name('show-add-kelas')
				->middleware('is_admin');
		Route::post('/post-data-kelas', 'KelasController@addKelas')
				->name('add-kelas')
				->middleware('is_admin');
		Route::get('/delete-data-kelas/{Kelas_id}', 'KelasController@DelKelas')
				->name('delete-kelas')
				->middleware('is_admin');
		Route::get('/show-edit-kelas/{Kelas_id}', 'KelasController@showEditKelas')
				->name('edit-kelas')
				->middleware('is_admin');
		Route::post('/update-kelas/{Kelas_id}', 'KelasController@updateKelas')
				->name('update-kelas')
				->middleware('is_admin');
	});
	
	// Kelola Siswa Kelas
	Route::prefix('siswakelas')->group(function(){
		// Kelola Data Siswa Kelas
		Route::get('/', 'SiswaKelasController@siswaKelas')
		->name('data-siswakelas')
		->middleware('is_admin');

		// Datatable Data Siswa Kelas
		Route::get('/datatable/1A', 'SiswaKelasController@siswaKelas1A')
		->name('data-siswakelas-1A')
		->middleware('is_admin');

		Route::get('/datatable/R', 'SiswaKelasController@siswaKelasR')
		->name('data-siswakelas-1A.riwayat')
		->middleware('is_admin');
		
		// Tambah
		Route::get('/tambah', 'SiswaKelasController@tambahSiswaKelas')
		->name('siswakelas.add')
		->middleware('is_admin');

		// Mutasi
		Route::get('/mutasi/{id}', 'SiswaKelasController@showAddSiswaKelas')
			->name('siswakelas.mutasi')
			->middleware('is_admin');
		
		Route::post('/post', 'SiswaKelasController@postAddSiswaKelas')
		->name('siswakelas.mutasi.post')
		->middleware('is_admin');

		Route::get('/delete/{id}', 'SiswaKelasController@deleteSiswaKelas')
			->name('siswakelas.delete')
			->middleware('is_admin');

	});

	// Kelola Data Guru
	Route::prefix('guru')->group(function(){
		// Kelola Data Siswa Kelas
		Route::get('/', 'GuruController@Guru')
		->name('data-guru')
		->middleware('is_admin');

		// Datatable Data Siswa Kelas
		Route::get('/datatable/guru', 'GuruController@guru1A')
		->name('data-Guru-1A')
		->middleware('is_admin');
		
		// Tambah
		Route::get('/tambah', 'GuruController@tambahGuru')
		->name('guru.add')
		->middleware('is_admin');

		// Mutasi
		Route::get('/edit/{id}', 'GuruController@showAddGuru')
			->name('guru.edit')
			->middleware('is_admin');
		
		Route::post('/post', 'GuruController@postAddGuru')
		->name('guru.post')
		->middleware('is_admin');

		Route::get('/delete/{id}', 'GuruController@deleteGuru')
			->name('guru.delete')
			->middleware('is_admin');


		// Export Import Excel
		Route::get('/export/excel/guru', 'GuruController@GuruExport')->middleware('is_admin')->name('guru-export');
		Route::post('/import/excel/guru', 'GuruController@GuruImport')->middleware('is_admin')->name('guru-import');

	});


});




// Export Import Siswa
Route::get('/export/siswakelas', 'SiswaKelasController@getExport');
Route::get('/import/siswakelas', 'SiswaKelasController@getImport');
Route::get('/export/excel/siswakelas', 'SiswaKelasController@SiswaKelasExport')->name('siswakelas-export');
Route::post('/import/excel/siswakelas', 'SiswaKelasController@SiswaKelasImport')->name('siswakelas-import');