@extends('layouts.app-admin')
@section('judul')
Mutasi Siswa
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Naik Kelas</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('data-siswakelas')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{route('siswakelas.mutasi.post')}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Nama Siswa</label>
                                            <select name="siswa_id" class="form-control" readonly>
                                                @foreach($siswa as $value => $data)
                                                @if($siswaK->siswa_id == $data->siswa_id)
                                                <option value="{{ $data->siswa_id }}" selected="">{{ $data->nama }}</option>
                                                @else 
                                                <option value="{{ $data->siswa_id }}">{{ $data->nama }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('siswa_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-4 col-xl-4">
                                            <label>Kelas</label>
                                            <select name="kelas_id" class="form-control">
                                                @foreach($kelas as $value => $data)
                                                    @if($siswaK->kelas_id == $data->kelas_id)
                                                    <option value="{{ $data->kelas_id }}" selected="">{{ $data->nama_kelas }} {{$data->kode_kelas}}</option>
                                                    @else 
                                                    <option value="{{ $data->kelas_id }}">{{ $data->nama_kelas }} {{$data->kode_kelas}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @error('kelas_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-4 col-xl-4">
                                            <label>Tahun Ajaran</label>
                                            <select name="tahun_ajaran_id" class="form-control">
                                                @foreach($tahun as $value => $data)
                                                @if($siswaK->tahun_ajaran_id == $data->tahun_ajaran_id)
                                                <option value="{{ $data->tahun_ajaran_id }}" selected="">{{ $data->tahun_ajaran_nama }}</option>
                                                @else 
                                                <option value="{{ $data->tahun_ajaran_id }}">{{ $data->tahun_ajaran_nama }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('tahun_ajaran_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        
                                    </div>
                                    <br>
                                    
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection