@extends('layouts.app-admin')
@section('judul')
kelola data siswa
@stop
@section('content-head')
<div class="col-md-12 mb-2">
    <div class="tab mt-5">
		<button class="tablinks" id="defaultOpen" onclick="openTabs(event, 'mutasi')">Data Siswa</button>
		<button class="tablinks" onclick="openTabs(event, 'riwayat')">Riwayat</button>
	</div>
</div>
@stop
@section('content')

<div id="mutasi" class="tabcontent">
<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola data siswa kelas</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('siswakelas.add')}}" class="btn btn-success text-white">
                                    <i class="icon-android-person-add text-white"></i>
                                    Tambah Siswa
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="siswaKelas1A" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Kelas</th>
                                                <th>Kode Kelas</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<div id="riwayat" class="tabcontent">
<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Riwayat siswa kelas</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="siswaKelasR" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Kelas</th>
                                                <th>Kode Kelas</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#siswaKelas1A').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('data-siswakelas-1A') !!}",
            columns: [
            {data: 'DT_RowIndex',name: 'siswa_kelas_id'},
            {data: 'nama',name: 'nama'},
            {data: 'nama_kelas',name: 'nama_kelas'},
            {data: 'kode_kelas',name: 'kode_kelas'},
            {data: 'action',name: 'action', width: 200},
            ]
        });
    });
</script>

<script>
    $(function() {
        $('#siswaKelasR').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('data-siswakelas-1A.riwayat') !!}",
            columns: [
            {data: 'DT_RowIndex',name: 'siswa_kelas_id'},
            {data: 'nama',name: 'nama'},
            {data: 'nama_kelas',name: 'nama_kelas'},
            {data: 'kode_kelas',name: 'kode_kelas'},
            ]
        });
    });
</script>

<script type="text/javascript">
function openTabs(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
@stop