@extends('layouts.app-admin')
@section('judul')
Add Kelas
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Kelas</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('kelola-kelas')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{route('add-kelas')}}">
                                    @csrf
                                    <input type="hidden" name="sekolah_id" value="{{auth::user()->sekolah_id}}">
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Kode Kelas</label>
                                            <input class="form-control" type="text" name="kode_kelas" value="{{ old('kode_kelas') }}" required="" placeholder="Masukan Kode Kelas">
                                            @error('kode_kelas')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <br>

                                        <div class="col-lg-12 col-xl-12">
                                            <label>Nama kelas</label>
                                            <input class="form-control" type="text" name="nama_kelas" value="{{ old('nama_kelas') }}" placeholder="Masukan Nama Kelas" required="">
                                            @error('nama_kelas')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-lg-12 col-xl-12">
                                            <label>Nama Sekolah</label>
                                            <select name="sekolah_id" class="form-control" required id="">
                                                @foreach($sekolah as $s)
                                                <option value="{{$s->sekolah_id}}">{{$s->nama}}</option>
                                                @endforeach
                                            </select>
                                            @error('sekolah_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection