@extends('layouts.app-admin')
@section('judul')
Edit Guru
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Guru</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('data-guru')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{route('guru.post')}}">
                                    @csrf
                                    <input type="hidden" name="guru_id" value="{{$guru->guru_id}}">
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Nama Guru</label>
                                            <input type="text" class="form-control" name="nama_guru" placeholder="Masukkan nama guru..." value="{{$guru->nama_guru}}">
                                            
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-4 col-xl-4">
                                            <label>NIP / NIK</label>
                                            <input type="text" class="form-control" name="nip" placeholder="Masukkan nip / nik guru..." value="{{$guru->nip}}">
                                            
                                        </div>
                                        <div class="col-lg-4 col-xl-4">
                                            <label>Jenis Kelamin</label>
                                            <select name="jenis_kelamin" id="" class="form-control">
                                                <option value="L">Laki - Laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                            
                                        </div>
                                        <div class="col-lg-4 col-xl-4">
                                            <label>Sekolah</label>
                                            <select name="sekolah_id" class="form-control">
                                                @foreach($sekolah as $value => $data)
                                                @if($guru->sekolah_id == $data->sekolah_id)
                                                <option value="{{$data->sekolah_id}}" selected="">{{$data->nama}}</option>
                                                @else
                                                <option value="{{$data->sekolah_id}}">{{$data->nama}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            
                                        </div>
                                        
                                    </div>
                                    <br>
                                    
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection