@extends('layouts.app-admin')

@section('judul')
kelola data guru
@stop

@section('content')

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h6 class="modal-title">Import Excel</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
            <form action="{{route('guru-import')}}" method="post" enctype="multipart/form-data">
                @csrf 
                <input type="file" name="file">
                <button>Import</button>
            </form>
        </div>
  

      </div>
    </div>
  </div>

<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola data guru</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('guru.add')}}" class="btn btn-success text-white">
                                    <i class="icon-android-person-add text-white"></i>
                                    Tambah Guru
                                </a>
                                <a href="{{route('guru-export')}}" class="btn btn-primary btn-sm"><i class="icon-android-download text-white"></i> Export</a>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal"><i class="icon-android-upload text-white"></i> Import</a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="guru" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>NIP</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Sekolah</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#guru').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('data-Guru-1A') !!}",
            columns: [
            {data: 'DT_RowIndex',name: 'guru_id'},
            {data: 'nama_guru',name: 'nama_guru'},
            {data: 'nip',name: 'nip'},
            {data: 'jenis_kelamin',name: 'jenis_kelamin'},
            {data: 'nama',name: 'nama'},
            {data: 'action',name: 'action', width: 200},
            ]
        });
    });
</script>


<script type="text/javascript">
function openTabs(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
@stop