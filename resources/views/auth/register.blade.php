<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Daftar</title>
    @include('layouts.assets-head')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
            <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                    <div class="card-header no-border">
                        <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Buat Akun</span></h6>
                    </div>
                    <div class="card-body collapse in"> 
                        <div class="card-block">
                            <form class="form-horizontal form-simple" method="POST" action="{{ route('Register-post') }}" novalidate>
                                @csrf
                                <fieldset class="form-group position-relative has-icon-left mb-1">
                                    <input type="text" class="form-control form-control-lg input-lg @error('name') is-invalid @enderror" id="user-name" placeholder="Masukan nama"  value="{{ old('name') }}" required autocomplete="name" autofocus name="name" required="">
                                    <div class="form-control-position">
                                        <i class="icon-head"></i>
                                    </div>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left" style="margin-top: -9px;">
                                    <input type="email" class="form-control form-control-lg input-lg @error('email') is-invalid @enderror" id="user-email" placeholder="Masukan Email" required name="email" value="{{ old('email') }}" required autocomplete="email" required="">
                                    <div class="form-control-position">
                                        <i class="icon-mail6"></i>
                                    </div>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <select class="form-control" name="sekolah">
                                        <option selected="" disabled="">Pilih Sekolah</option>
                                        @foreach($S as $s)
                                            <option value="{{$s->sekolah_id}}">
                                                {{$s->nama}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="form-control-position">
                                        <i class="icon-android-options"></i>
                                    </div>
                                    @error('sekolah')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="password" class="form-control form-control-lg input-lg  @error('password') is-invalid @enderror" id="user-password" placeholder="Masukan Password" required name="password" required autocomplete="new-password" required="">
                                    <div class="form-control-position">
                                        <i class="icon-key3"></i>
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left" style="margin-top: -15px;">
                                    <input type="password" class="form-control form-control-lg input-lg"  type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Masukan Ulang Password" required="">
                                    <div class="form-control-position">
                                        <i class="icon-key3"></i>
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-info btn-lg btn-block"><i class="icon-unlock2"></i> Daftar</button>
                            </form>
                        </div>
                        <p class="text-xs-center">Sudah Punya Akun ? <a href="login" class="card-link">Masuk</a></p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</div>
@include('layouts.assets-footer')
</body>
</html>
