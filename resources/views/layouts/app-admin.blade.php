<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>@yield('judul')</title>
  <style>
  .tab {
  overflow: hidden;x
  border: 1px solid #ccc;
  border-top: none;
  border-right: none;
  border-left: none;
  background-color: #F6F7FA;
  }

  /* Style the buttons inside the tab */
  .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {
    background-color: #ddd;
  }

  /* Create an active/current tablink class */
  .tab button.active {
    background-color: #2f56c8;
    color: #fff;
  }

  /* Style the tab content */
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border-top: none;
  }
</style>
  @include('layouts.assets-head')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

  <!-- navbar-fixed-top-->
  <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav">
          <li class="nav-item mobile-menu hidden-md-up float-xs-left">
            <a class="nav-link nav-menu-main menu-toggle hidden-xs">
              <i class="icon-menu5 font-large-1"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.html" class="navbar-brand nav-link">
              <img alt="branding logo" src="../../app-assets/images/logo/robust-logo-light.png" data-expand="../../app-assets/images/logo/robust-logo-light.png" data-collapse="../../app-assets/images/logo/robust-logo-small.png" class="brand-logo">
            </a>
          </li>
          <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
        </ul>
      </div>
      <div class="navbar-container content container-fluid">
        <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
          <ul class="nav navbar-nav">
            <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5">         </i></a></li>
            <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
          </ul>
          <ul class="nav navbar-nav float-xs-right">
            <li class="dropdown dropdown-user nav-item">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link" style="margin-top: 10px;">
                <span>{{auth::user()->name}}</span></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <i class="icon-power3"></i> {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- ////////////////////////////////////////////////////////////////////////////-->


  <!-- main menu-->
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu header-->
    <div class="main-menu-header">
      <hr>
    </div>
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content">
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <li class=" nav-item">
          <a href="{{route('home')}}"><i class="icon-android-home"></i>
            <span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">
              Home
            </span>
          </a>
        </li>
        <li class=" nav-item"><a href="#"><i class="icon-android-funnel"></i><span data-i18n="nav.menu_levels.main" class="menu-title">Kelola Data</span></a>
          <ul class="menu-content">
            <li>
              <a href="{{url('admin/kelola-data-sekolah/'. auth::user()->sekolah_id)}}" data-i18n="nav.menu_levels.second_level" class="menu-item">Kelola Sekolah</a>
            </li>
            <li>
              <a href="{{route('kelola-tahun-ajaran')}}" data-i18n="nav.menu_levels.second_level" class="menu-item">Kelola Tahun Ajaran</a>
            </li>
            <li>
              <a href="{{route('kelola-kelas')}}" data-i18n="nav.menu_levels.second_level" class="menu-item">Kelola kelas</a>
            </li>
            <li>
              <a href="{{route('kelola-data-tahunajaran')}}" data-i18n="nav.menu_levels.second_level" class="menu-item">Tahun Ajaran</a>
            </li>
            <li>
              <a href="{{route('data-guru')}}" data-i18n="nav.menu_levels.second_level" class="menu-item">Kelola guru</a>
            </li>
            <li><a href="#" data-i18n="nav.menu_levels.second_level_child.main" class="menu-item">Kelola siswa</a>
              <ul class="menu-content">
                <li><a href="{{route('kelola-data-siswa')}}" data-i18n="nav.menu_levels.second_level_child.third_level" class="menu-item">Data siswa</a>
                </li>
                <li><a href="{{route('data-siswakelas')}}" data-i18n="nav.menu_levels.second_level_child.third_level" class="menu-item">Siswa kelas</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title">@yield('content-head')</h2>
        </div>
      </div>
      <div class="content-body"><!-- Statistics -->
        @yield('content')
      </div>
    </div>
  </div>

  @include('layouts.footer')
  @include('layouts.assets-footer')

  @yield('scripts')
</body>
</html>
