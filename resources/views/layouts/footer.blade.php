<footer class="footer footer-static footer-light navbar-border" style="bottom: 0; position: fixed; width: 100%;">
    <p class="clearfix text-muted text-sm-center mb-0 px-2">
      <span class="float-md-left d-xs-block d-md-inline-block"> 
        <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">
          Footer
        </a>
      </span>
      <span class="float-md-right d-xs-block d-md-inline-block">
      </span>
    </p>
  </footer>