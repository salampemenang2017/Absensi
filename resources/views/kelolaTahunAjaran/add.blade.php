@extends('layouts.app-admin')
@section('judul')
Add Tahun
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Tahun</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('kelola-tahun-ajaran')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{route('add-tahun')}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Tahun</label>
                                            <input class="form-control" type="text" name="tahun_ajaran_nama" value="{{ old('tahun_ajaran_nama') }}" required="" placeholder="Masukan nama Sekolah">
                                            @error('tahun_ajaran_nama')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6 col-xl-6">
                                            <label>status</label>
                                            <select name="status" class="form-control">
                                                <option value="aktif">aktif</option>
                                                <option value="tidal aktif">tidak aktif</option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection