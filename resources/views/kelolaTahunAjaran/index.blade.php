@extends('layouts.app-admin')
@section('judul')
kelola data kelas
@stop
@section('content-head')
Kelola data tahun ajaran
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola data tahun ajaran</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('show-add-tahun')}}" class="btn btn-success text-white">
                                    <i class="icon-android-person-add text-white"></i>
                                    Tambah Tahun
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="data_adminS_side" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Tahun ajaran</th>
                                                <th>status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#data_adminS_side').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('get-tahun-ajaran') !!}",
            columns: [
            {data: 'tahun_ajaran_id',name: 'tahun_ajaran_id'},
            {data: 'tahun_ajaran_nama',name: 'tahun_ajaran_nama'},
            {data: 'status',name: 'status'},
            {data: 'action',name: 'action'},
            ]
        });
    });
</script>
@stop