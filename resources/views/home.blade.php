@extends('layouts.app-admin')

@section('content')
<div class="container">
    @if (\Session::has('error'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-lg-12 col-xl-12">
            <div class="alert alert-info alert-dismissible fade in mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Selamat datang!</strong><a href="#" class="alert-link text-white"> {{auth::user()->name}}</a>   
                Di halaman Home 
            </div>
        </div>
    </div>
</div>
@endsection
