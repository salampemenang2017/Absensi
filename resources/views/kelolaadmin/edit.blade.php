@extends('layouts.app-superadmin')
@section('judul')
Edit AdminS
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">
            
        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Admins</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('super.admin.home')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="container">
                                    <form method="post" action="{{url('/update-admins/'. $Admins->id)}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6 col-xl-6">
                                                <label>Nama</label>
                                                <input class="form-control" type="text" name="name" value="{{$Admins->name}}" required="" placeholder="Masukan nama">
                                            </div>
                                            <div class="col-lg-6 col-xl-6">
                                                <label>Email</label>
                                                <input class="form-control" type="text" name="email" value="{{$Admins->email}}" required="" placeholder="Masukan email">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-6 col-xl-6">
                                                <label>Nip</label>
                                                <input class="form-control" type="text" name="nip" value="{{$Admins->nip}}" required="" placeholder="Masukan Nip">

                                            </div>
                                            <div class="col-xl-6 col-lg-6">
                                                <label>Jenis Kelamin</label>
                                                <select class="form-control" name="jenis_kelamin" required="" value="{{$Admins->jenis_kelamin}}">
                                                    <option value="Laki laki">Laki-Laki</option>
                                                    <option value="perempuan">Perempuan</option>
                                                </select>        
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12">
                                                <label>Alamat</label>
                                                <textarea style="height: 150px;" class="form-control" required="" name="alamat">{{$Admins->alamat}}</textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12">
                                                <label>Nama Sekolah</label>
                                                <select class="form-control" required="" name="sekolah">
                                                    @foreach($Sekolah as $s)
                                                        <option value="{{$s->sekolah_id}}">
                                                            {{$s->nama}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <input class="btn btn-info" type="submit" value="Update">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">
            
        </div>
    </div>
</section>
<br><br><br>
@endsection