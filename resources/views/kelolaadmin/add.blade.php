@extends('layouts.app-superadmin')
@section('judul')
Add AdminS
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Admins</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('super.admin.home')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{url('/add-post-admins/')}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Nama</label>
                                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" required="" placeholder="Masukan nama">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email" value="{{ old('email') }}" required="" placeholder="Masukan email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Nip</label>
                                            <input class="form-control" type="text" name="nip" value="{{ old('nip') }}" required="" placeholder="Masukan Nip">
                                            @error('nip')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <label>Jenis Kelamin</label>
                                            <select class="form-control" name="jenis_kelamin" required="" value="{{ old('jenis_kelamin') }}">
                                                <option selected="" disabled="">Pilih Jenis kelamin</option>
                                                <option value="Laki laki">Laki-Laki</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>       
                                            @error('jenis_kelamin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror 
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12">
                                            <label>Alamat</label>
                                            <textarea style="height: 150px;" class="form-control" required="" name="alamat">{{ old('alamat') }}</textarea>
                                            @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12">
                                            <label>Nama Sekolah</label>
                                            <select class="form-control" required="" name="sekolah" value="{{ old('sekolah') }}">
                                                <option selected="" disabled="">
                                                    pilih sekolah
                                                </option>
                                                @foreach($Sekolah as $s)
                                                <option value="{{$s->sekolah_id}}">
                                                    {{$s->nama}}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('sekolah')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control" required="">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <label>Ulangi Password</label>
                                            <input type="password" name="password_confirmation" class="form-control" required="">
                                            @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection