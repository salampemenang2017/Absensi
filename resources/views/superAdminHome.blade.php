@extends('layouts.app-superadmin')
@section('judul')
Home
@stop
@section('content-head')
Kelola Akun
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola Akun Admin Sekolah</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('add-Admin')}}" class="btn btn-success text-white">
                                    <i class="icon-android-person-add text-white"></i>
                                    Tambah Admin
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="data_adminS_side" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Nip</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Alamat</th>
                                                <th>Sekolah</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#data_adminS_side').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "/adminS_json",
            columns: [
            {data: 'id',name: 'id'},
            {data: 'name',name: 'name'},
            {data: 'email',name: 'email'},
            {data: 'nip',name: 'nip'},
            {data: 'jenis_kelamin',name: 'jenis_kelamin'},
            {data: 'alamat',name: 'alamat'},
            {data: 'nama',name: 'nama'},
            {data: 'action',name: 'action'},
            ]
        });
    });
</script>
@stop