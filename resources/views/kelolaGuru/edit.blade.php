@extends('layouts.app-admin')
@section('judul')
Edit Guru
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Guru</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('kelola-guru')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{url('admin/update-guru/'. $guru->guru_id)}}">
                                    @csrf
                                    <input type="hidden" name="sekolah_id" value="{{auth::user()->sekolah_id}}">
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Nama</label>
                                            <input class="form-control" type="text" name="nama_guru" value="{{ $guru->nama_guru }}" required="" placeholder="Masukan nama guru">
                                            @error('nama_guru')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <label>nip</label>
                                            <input class="form-control" type="text" name="nip" value="{{ $guru->nip }}" placeholder="Masukan Nip" required="">
                                            @error('nip')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Jenis Kelamin</label>
                                            <select value="{{$guru->jenis_kelamin}}" name="jenis_kelamin" class="form-control">
                                                <option value="L">Laki Laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                            @error('nip')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Alamat</label>
                                            <textarea name="alamat" class="form-control" style="height: 150px;">{{$guru->alamat}}</textarea>
                                            @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection