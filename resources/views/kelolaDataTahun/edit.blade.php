@extends('layouts.app-admin')
@section('judul')
Edit tahun
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Tahun Ajaran</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('kelola-data-tahunajaran')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{url('admin/tahunajaran/update-tahunajaran/'. $tahun->tahun_ajaran_id)}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Tahun Ajaran</label>
                                            <input class="form-control" type="text" name="tahun_ajaran_nama" value="{{$tahun->tahun_ajaran_nama}}" required="" placeholder="Masukan Tahun Ajaran">
                                            @error('tahun_ajaran_nama')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-lg-12 col-xl-12">
                                            <label>Status</label>
                                            <select name="status" id="" value="{{ old('status') }}" class="form-control" required="">
                                                <option value="">Pilih Status</option>
                                                <option value="Aktif">Aktif</option>
                                                <option value="Non aktif">Non Aktif</option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                        
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection