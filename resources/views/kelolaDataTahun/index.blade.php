@extends('layouts.app-admin')
@section('judul')
kelola data tahun ajaran
@stop

@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola data tahun ajaran</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('show-add-tahunajaran')}}" class="btn btn-success text-white">
                                    <i class="icon-android-person-add text-white"></i>
                                    Tambah Data
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="data_adminS_side" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tahun Ajaran</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#data_adminS_side').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('get-data-tahunajaran') !!}",
            columns: [
            {data: 'DT_RowIndex',name: 'tahun_ajaran_id'},
            {data: 'tahun_ajaran_nama',name: 'tahun_ajaran_nama'},
            {data: 'status',name: 'status'},
            {data: 'action',name: 'action', width: 200},
            ]
        });
    });
</script>
@stop