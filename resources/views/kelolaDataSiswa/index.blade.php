@extends('layouts.app-admin')

@section('judul')
kelola data siswa
@stop

@section('content')
<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h6 class="modal-title">Import Excel</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
            <form action="{{route('siswa-import')}}" method="post" enctype="multipart/form-data">
                @csrf 
                <input type="file" name="file">
                <button>Import</button>
            </form>
        </div>
  

      </div>
    </div>
  </div>
  

<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola data siswa</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('show-add-siswa')}}" class="btn btn-success text-white">
                                    <i class="icon-android-person-add text-white"></i>
                                    Tambah Siswa
                                </a>
                                <a href="{{route('siswa-export')}}" class="btn btn-primary btn-sm"><i class="icon-android-download text-white"></i> Export</a>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal"><i class="icon-android-upload text-white"></i> Import</a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="data_adminS_side" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Nis</th>
                                                <th>Nisn</th>
                                                <th>Nik</th>
                                                <th>No Akte</th>
                                                <th>No kk</th>
                                                <th>jenis Kelamin</th>
                                                <th>Agama</th>
                                                <th>Alamat</th>
                                                <th>No Hp Orang tua</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#data_adminS_side').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('get-data-siswa') !!}",
            columns: [
            {data: 'siswa_id',name: 'siswa_id'},
            {data: 'nama',name: 'nama'},
            {data: 'nis',name: 'nis'},
            {data: 'nisn',name: 'nisn'},
            {data: 'nik',name: 'nik'},
            {data: 'no_akte',name: 'no_akte'},
            {data: 'no_kk',name: 'no_kk'},
            {data: 'jenis_kelamin',name: 'jenis_kelamin'},
            {data: 'agama',name: 'agama'},
            {data: 'alamat',name: 'alamat'},
            {data: 'no_hp_ortu',name: 'no_hp_ortu'},
            {data: 'action',name: 'action', width: 200},
            ]
        });
    });
</script>
@stop