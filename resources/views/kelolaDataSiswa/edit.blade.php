@extends('layouts.app-admin')
@section('judul')
Edit Siswa
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Siswa</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('kelola-data-siswa')}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{url('admin/siswa/update-siswa/'. $siswa->siswa_id)}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-12">
                                            <label>Nama Siswa</label>
                                            <input class="form-control" type="text" name="nama" value="{{$siswa->nama}}" required="" placeholder="Masukan nama Sekolah">
                                            @error('nama')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-4 col-xl-4">
                                            <label>Nis</label>
                                            <input class="form-control" type="text" name="nis" value="{{$siswa->nis}}" placeholder="Masukan Nis" required="">
                                            @error('nis')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-4 col-xl-4">
                                            <label>Nisn</label>
                                            <input class="form-control" type="text" name="nisn" value="{{$siswa->nisn}}" placeholder="Masukan Nisn" required="">
                                            @error('nisn')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-4 col-lg-4">
                                            <label>Nik</label>
                                            <input value="{{$siswa->nik}}" type="text" class="form-control" name="nik" placeholder="Masukan nik">
                                            @error('nik')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror 
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-4 col-lg-4">
                                            <label>no akte</label>
                                            <input type="text" name="no_akte" class="form-control" value="{{$siswa->no_akte}}" placeholder="Masukan no akte">
                                            @error('no_akte')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-4 col-lg-4">
                                            <label>no kk</label>
                                            <input type="text" name="no_kk" class="form-control" value="{{$siswa->no_kk}}" placeholder="Masukan no kk">
                                            @error('no_kk')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-4 col-lg-4">
                                            <label>Jenis kelamin</label>
                                            <select name="jenis_kelamin" class="form-control" value="{{$siswa->jenis_kelamin}}" required="">
                                                <option value="L">Laki Laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                            @error('jenis_kelamin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6">
                                            <label>Agama</label>
                                            <input value="{{$siswa->agama}}" type="text" name="agama" class="form-control" required="">
                                            @error('agama')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <label>No hp Orang Tua</label>
                                            <input type="text" name="no_hp_ortu" class="form-control" required="" value="{{$siswa->no_hp_ortu}}">
                                            @error('no_hp_ortu')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12">
                                            <label>Alamat</label>
                                            <textarea name="alamat" class="form-control">{{$siswa->alamat}}</textarea>
                                            @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection