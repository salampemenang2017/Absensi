@extends('layouts.app-superadmin')

@section('judul')
kelola Sekolah
@stop

@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Kelola Sekolah</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{route('show.tambah-data-sekolah')}}" class="btn btn-success text-white">
                                    <i class="icon-android-add text-white"></i>
                                    Tambah Sekolah
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <div class="table-responsive">
                                    <table id="data_adminS_side" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Npsn</th>
                                                <th>Kode Un</th>
                                                <th>Alamat</th>
                                                <th>No Sk Pendirian</th>
                                                <th>Tanggal Pendirian</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br><br><br>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#data_adminS_side').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: "{!! route('get-data-sekolah') !!}",
            columns: [
            {data: 'sekolah_id',name: 'sekolah_id'},
            {data: 'nama',name: 'nama'},
            {data: 'npsn',name: 'npsn'},
            {data: 'kode_un',name: 'kode_un'},
            {data: 'alamat',name: 'alamat'},
            {data: 'no_sk_pendirian_sekolah',name: 'no_sk_pendirian_sekolah'},
            {data: 'tgl_pendirian',name: 'tgl_pendirian'},
            {data: 'action',name: 'action', width: 100},
            ]
        });
    });
</script>
@stop