@extends('layouts.app-admin')
@section('judul')
Edit Sekolah
@stop
@section('content')
<section id="card-actions">
    <div class="row">
        <div class="col-xs-2">

        </div>
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Sekolah</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{url('admin/kelola-data-sekolah/'. $sekolah->sekolah_id)}}" class="btn btn-danger text-white">
                                    Kembali
                                </a>
                            </li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <form method="post" action="{{url('admin/update-data-persekolah/'. $sekolah->sekolah_id)}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Nama Sekolah</label>
                                            <input class="form-control" type="text" name="nama" value="{{$sekolah->nama}}" required="" placeholder="Masukan nama Sekolah">
                                            @error('nama')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Npsn</label>
                                            <input class="form-control" type="text" name="npsn" value="{{$sekolah->npsn}}" placeholder="Masukan npsn">
                                            @error('npsn')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <label>Kode Un</label>
                                            <input class="form-control" type="text" name="kode_un" value="{{$sekolah->kode_un}}" placeholder="Masukan kode un">
                                            @error('kode_un')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <label>No Sk Pendirian Sekolah</label>
                                            <input value="{{$sekolah->no_sk_pendirian_sekolah}}" type="text" class="form-control" name="no_sk_pendirian_Sekolah" placeholder="Masukan no_sk_pendirian_Sekolah">
                                            @error('no_sk_pendirian_Sekolah')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror 
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12">
                                            <label>Alamat</label>
                                            <textarea style="height: 150px;" class="form-control" name="alamat">{{$sekolah->alamat}}</textarea>
                                            @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6">
                                            <label>Tanggal Pedirian</label>
                                            <input type="date" value="{{$sekolah->tgl_pendirian}}" name="tgl_pendirian" class="form-control">
                                            @error('tgl_pendirian')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                        </div>
                                    </div>
                                    <br>
                                    <input class="btn btn-info" type="submit" value="Save">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
</section>
@endsection