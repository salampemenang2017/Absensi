@extends('layouts.app-admin')
@section('judul')
Data Sekolah
@stop
@section('content')
<section id="timeline" class="timeline-left timeline-wrapper">
    <div class="card border-grey border-lighten-2">
        <div class="card-header">
            <div class="heading-elements">
                <ul class="list-inline">
                    <li>
                        <form method="post" action="{{url('admin/update-logo/'. $sekolah->sekolah_id)}}" enctype="multipart/form-data">
                            @csrf
                            <label for="logo" class="text-muted">
                                <span class="icon-edit"></span> 
                                Ganti foto
                            </label>
                            <input accept="image/png,image/jpeg,image/*" onchange="this.form.submit();" id="logo" type="file" name="logo" style="display: none;">
                        </form>
                    </li>
                    <li>
                        <a href="{{url('admin/edit-data-persekolah/'. $sekolah->sekolah_id)}}">
                            <i class="icon-edit"></i> 
                            Edit Data Sekolah
                        </a>
                    </li>
                    <li><a data-action="reload"><i class="icon-repeat2"></i></a></li>
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <div class="row">
                    <div class="col-lg-4 col-xs-12">
                        <img class="img-fluid" src="{{'/img/'. $sekolah->logo}}" style="width: 100%; height: 100%;">
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <h2>{{$sekolah->nama}}</h2>
                        <h3 class="my-1">{{$sekolah->tgl_pendirian}}</h3>
                        <p class="lead">
                            <span>Npsn : {{$sekolah->npsn}}</span>
                        </p>
                        <p class="lead">
                            <span>Kode Un : {{$sekolah->kode_un}}</span>
                        </p>
                        <p class="lead">
                            <span>No Sk Pendirian : {{$sekolah->no_sk_pendirian_Sekolah}}</span>
                        </p>
                        <p>
                            <strong>
                                <center>Alamat</center>
                            </strong>
                            <hr>
                            <span>
                                <center>
                                    {{$sekolah->alamat}}
                                </center>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection